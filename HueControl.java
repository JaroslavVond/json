package Hue;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import com.philips.lighting.hue.sdk.utilities.PHUtilities;
import com.philips.lighting.model.PHLightState;

public class HueControl {


	private String GetInfoAboutLight(String id) {

		String info = "";
		JSONObject myResponse = SendHTTP("/lights/" + id, "GET", "");

		try {

			//String state = myResponse.getJSONObject("state").getString("on");

		} catch (Exception ex) {
			System.out.println("error - " + ex);
			return null;
		}

		return null;

	}

	private JSONObject SendHTTP(String urlS, String method, String body) {

		try {

			String url = "http://" + ipAddrBridge + "/api/" + user + urlS;
			
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod(method);
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			
			if (!body.equals("")) {
				OutputStream os = con.getOutputStream();
				OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
				osw.write(body);
				osw.flush();
				osw.close();
				os.close(); // don't forget to close the OutputStream
			}
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}

			in.close();

			System.out.println(response.toString());
			return new JSONObject(response.toString());

		} catch (Exception ex) {
			System.out.println(ex);
			return null;
		}

	}

}
